#include "example.h"

Example::Example()
{
    pointer_ = new int(-1);
#ifdef DEBUG
    cout << "Example class default constructor called\n";
#endif
}

Example::Example(int value)
{
    pointer_ = new int(value);
#ifdef DEBUG
    cout << "Example class one-arg constructor called\n";
#endif
}

Example::Example(const Example &old)
{
    int value = *(old.pointer_);
    pointer_ = new int(value);
#ifdef DEBUG
    cout << "Example class copy constructor called\n";
#endif
}//copy constructor

void Example::print()
{
    cout << "Int value = " << *pointer_ << ", address = " << pointer_ << endl;
}

int Example::getIntValue() const
{
    return *pointer_;
}

Example::~Example()
{
#ifdef DEBUG
    cout << "Example class destructor called, address " << pointer_ << " is now invalid\n";
#endif
    delete pointer_;
    pointer_ = NULL;
}

Example Example::operator+(const Example &rhs)
{
        int result = *(this->pointer_) + *(rhs.pointer_);
        Example object(result);
        return object;
}

Example &Example::operator =(const Example &rhs)
{
//    this->pointer_ = rhs.pointer_;
    if(this != &rhs) {//avoid self-assignment

    *(this->pointer_) = *(rhs.pointer_);
    }
#ifdef DEBUG
    cout << "Example class overloaded assignment operator called\n";
#endif

    return *this;//to facilitate chaining

}

