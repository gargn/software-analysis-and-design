#ifndef STUDENT_H
#define STUDENT_H
#include "person.h"

class Student : public Person //is-a relationship
{

public:
    Student();
    Student(int id, string name, double gpa, string major);
    double gpa() const; //accessor for gpa_
    string major() const; //accessor for major_
    void print() const;//override base class method

private:
    double gpa_;
    string major_;
};

#endif // STUDENT_H
