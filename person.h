#ifndef PERSON_H
#define PERSON_H
#include <iostream>
#define DEBUG

using namespace std;

//abstract class
class Person
{

public:
    Person();//default constructor
    Person(int id, string name);
    Person(string name);
    string name() const;//accessor for name
    int id() const;//access for id
    void name(string name);//mutator for name
    void id(int id);//mutator for id
    char typeInfo() const;
    virtual void print() const =0;//pure virtual function, to be overridden in derived classes
protected :
    string name_;
    int id_;
    char typeInfo_;
};

#endif // PERSON_H
