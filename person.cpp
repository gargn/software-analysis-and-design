#include "person.h"

Person::Person()
{
    id_=0;
    name_ = "John";
    typeInfo_='p';
#ifdef DEBUG
    cout << "Person class default constructor called\n";
#endif
}

Person::Person(int id, string name):id_(id),name_(name)
{
    typeInfo_='p';
 #ifdef DEBUG
     cout << "Person class 2-arg constructor called\n";
 #endif
}

Person::Person(string name):name_(name)
{
   id_=0;
   typeInfo_='p';
#ifdef DEBUG
    cout << "Person class 1-arg constructor called\n";
#endif
}

string Person::name() const
{
    return name_;
}

int Person::id() const
{
    return id_;
}

void Person::name(string name)
{
    name_ = name;
}

void Person::id(int id)
{
    id_=id;
}

char Person::typeInfo() const
{
    return typeInfo_;
}

void Person::print() const
{
    cout << "Person : id = " << id_ << ", name = " << name_ << " -> ";
}



