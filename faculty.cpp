#include "faculty.h"

Faculty::Faculty():department_("ECE"),salary_(90000)
{
    id_=-1;
    name_="Faculty";
    typeInfo_='f';

#ifdef DEBUG
    cout << "Faculty class default constructor called\n";
#endif
}

Faculty::Faculty(int id, string name, string department, double salary):department_(department),salary_(salary),Person(id,name)
{
    typeInfo_='f';
#ifdef DEBUG
    cout << "Faculty class 4-arg constructor called\n";
#endif
}

string Faculty::department() const
{
 return department_;
}

double Faculty::salary() const
{
    return salary_;
}

void Faculty::print() const
{
    Person::print();
    cout << "Faculty: department = " << department_ << ", salary = " << salary_ << endl;
}

