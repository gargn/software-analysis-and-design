#ifndef FACULTY_H
#define FACULTY_H
#include "person.h"

class Faculty : public Person
{
public:
    Faculty();
    Faculty(int id, string name, string department, double salary);
    string department() const;//accessor for department_
    double salary() const;
    void print() const; //overriding base class method

private:
    string department_;
    double salary_;
};

#endif // FACULTY_H
