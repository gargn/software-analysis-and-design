QT += core
QT -= gui

TARGET = Spring2016
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    person.cpp \
    student.cpp \
    faculty.cpp \
    course.cpp \
    example.cpp

HEADERS += \
    person.h \
    student.h \
    faculty.h \
    course.h \
    example.h

