#include <iostream>
#include "example.h"

using namespace std;

//Example operator +(const Example &lhs, const Example &rhs)
//{
//    int result = *(lhs.pointer_) + *(rhs.pointer_);
//    int result = lhs.getIntValue() + rhs.getIntValue();
//    Example object(result);
//    return object;
//}


int main(int argc, char *argv[])
{
    Example object1(4), object2(7);
    Example object3(6);

    object1.print();
    object2.print();
    object3.print();

//    object1 = object3 = object2;
    object1 = object1;
    object3.print();
    object1.print();


    return 0;
}



