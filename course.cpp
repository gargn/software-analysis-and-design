#include "course.h"

Course::Course():array_(NULL),size_(0),count_(0)
{
#ifdef DEBUG
    cout << "Course class default constructor called\n";
#endif
}

Course::Course(int size):size_(size),count_(0)
{
    array_ = new Person*[size];
#ifdef DEBUG
    cout << "Course class 1-arg constructor called\n";
#endif
}

Course::Course(const Course &oldObject)
{
#ifdef DEBUG
    cout << "Course class Copy constructor called\n";
#endif
    this->size_ = oldObject.size_;
    this->count_ = oldObject.count_;

    this->array_ = new Person*[size_];

    for (int i=0; i<count_; i++){
        if(oldObject.array_[i]->typeInfo()=='s')
        {
            Student* oldStudentPtr = static_cast<Student*>(oldObject.array_[i]);
            Student* studentPtr = new Student(*oldStudentPtr);
            this->array_[i] = studentPtr;
        }
        else
        {
            Faculty* oldFacultyPtr = static_cast<Faculty*>(oldObject.array_[i]);
            Faculty* facultyPtr = new Faculty( *oldFacultyPtr );
            this->array_[i] = facultyPtr;
        }
    }
}

void Course::addPerson(Person *person)
{
    if(count_<size_){
        array_[count_]=person;
    #ifdef DEBUG
        cout << "Person added at index " << count_ << endl;
    #endif
        ++count_;
    }
    else{
    #ifdef DEBUG
        cout << "Array is full\n";
    #endif
    }
}

void Course::print() const
{
    for(int i=0; i<count_; ++i)
    {
        array_[i]->print();
    }
}


