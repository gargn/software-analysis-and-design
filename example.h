#ifndef EXAMPLE_H
#define EXAMPLE_H

#include <iostream>
#define DEBUG

using namespace std;

class Example
{
//friend Example operator +(const Example &lhs, const Example &rhs);

public:
    Example();
    Example(int value);
    Example(const Example &old);
    void print();
    int getIntValue() const;
    ~Example();
    Example operator+ (const Example &rhs);
    Example& operator =(const Example &rhs);
private:
    int* pointer_;
};

#endif // EXAMPLE_H
