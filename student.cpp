#include "student.h"

Student::Student():gpa_(0),major_("CPE")
{
    id_=-1;
    name_="Student";
    typeInfo_='s';
#ifdef DEBUG
    cout << "Student class default constructor called\n";
#endif
}

//Student::Student(int id, string name, double gpa, string major):gpa_(gpa),major_(major)
//{
//    id_=id;
//    name_=name;
//#ifdef DEBUG
//    cout << "Student class 4-arg constructor called\n";
//#endif
//}

Student::Student(int id, string name, double gpa, string major):gpa_(gpa),major_(major),Person(id,name)
{
    typeInfo_='s';
#ifdef DEBUG
    cout << "Student class 4-arg constructor called\n";
#endif
}

double Student::gpa() const
{
    return gpa_;

}

string Student::major() const
{
    return major_;
}

void Student::print() const
{
    Person::print();//calling print method of base class
    cout << "Student: gpa = " << gpa_ << ", major = " << major_ << endl;
}

