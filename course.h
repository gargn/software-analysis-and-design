#ifndef COURSE_H
#define COURSE_H
//#include "person.h"

#include "student.h"
#include "faculty.h"

class Course
{
public:
    Course();
    Course(int size);
    Course (const Course &oldObject);//copy constructor
    void addPerson(Person* person); //add Person to the array
    void print() const; //print all information in the array


private:
    Person** array_; //pointer to pointer (to create a dynamic array of pointers)
    int size_;
    int count_;

};

#endif // COURSE_H
